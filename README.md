Program sklada sie z wielu klas oraz plikow konfiguracyjnych, dlatego prosta kompilacja poleceniem javac
jest niewygodna. Przygotowalem sie do cwiczenia i wiekszosc pracy wykonalem jeszcze w lipcu. Program korzysta z narzedzia Gradle i jest budowany jednym prostym
poleceniem. Nie potrzeba zadnych dodatkowych narzedzi poza Java 8.

Program jest serwerem umozliwiajacym modyfikacje bazy danych. 

Wszystkie poniższe polecenia należy uruchamiać w katalogu ze zrodlami

Budowa aplikacji Windows
```sh
gradlew build
```

Budowa aplikacji Linux
```sh
./gradlew build
```

Program domyslnie ma skonfigurowane poniższe polacznie do bazy dancyh, czyli
szuka pliku sqlite.db w katalogu projektu. Dolaczylem przykladowa baze do zrodel
```
jdbc:sqlite:sqlite.db
```

Uruchamianie aplikacji
```sh
java -jar build/libs/dbaccess-server-0.0.1-SNAPSHOT.jar
```

LIsta operacji dostepnych do wykonania na bazie danych jest dostepna pod adresem:
```
http://localhost:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
```