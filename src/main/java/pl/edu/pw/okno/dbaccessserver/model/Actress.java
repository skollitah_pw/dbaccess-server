package pl.edu.pw.okno.dbaccessserver.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity(name = "Aktorka")
public class Actress {

  @Id
  @Column(name = "Id")
  @Schema(example = "1", description = "Id of actress")
  private Integer id;

  @Column(name = "Nazwisko")
  @Schema(example = "Diane Kruger", description = "Actress's name")
  private String name;

  @Column(name = "[Rok Urodzenia]")
  @Schema(example = "1956", description = "Actress's year of birth")
  private int yearOfBirth;
}
