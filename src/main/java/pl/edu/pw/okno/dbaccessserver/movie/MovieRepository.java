package pl.edu.pw.okno.dbaccessserver.movie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pw.okno.dbaccessserver.model.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {}
