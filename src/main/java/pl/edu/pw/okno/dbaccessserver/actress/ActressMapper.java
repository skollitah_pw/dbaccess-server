package pl.edu.pw.okno.dbaccessserver.actress;

import org.springframework.stereotype.Controller;
import pl.edu.pw.okno.dbaccessserver.model.Actress;

import java.util.Random;

@Controller
public class ActressMapper {

  public Actress map(ActressForm actressForm) {
    return Actress.builder()
        .id(new Random().nextInt())
        .name(actressForm.getName())
        .yearOfBirth(actressForm.getYearOfBirth())
        .build();
  }

  public Actress merge(Actress actress, ActressForm actressForm) {
    actress.setName(actressForm.getName());
    actress.setYearOfBirth(actressForm.getYearOfBirth());
    return actress;
  }
}
