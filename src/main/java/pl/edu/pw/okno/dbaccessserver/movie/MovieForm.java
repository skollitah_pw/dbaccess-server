package pl.edu.pw.okno.dbaccessserver.movie;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class MovieForm {

  @Schema(example = "Blade Runner", description = "Title of the movie")
  @NotBlank
  private String title;

  @Schema(example = "2018", description = "Year of production")
  @NotNull
  @Positive
  private int year;

  @Schema(example = "Robert Zemeckis", description = "Director's name")
  @NotBlank
  private String directorName;

  @Schema(example = "Michelle Pfeiffer", description = "Actress's name")
  @NotBlank
  private String actressName;
}
