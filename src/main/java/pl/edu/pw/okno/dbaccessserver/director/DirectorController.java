package pl.edu.pw.okno.dbaccessserver.director;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.edu.pw.okno.dbaccessserver.model.Director;

import javax.persistence.TableGenerator;
import java.util.List;

@Slf4j
@TableGenerator(name = "Director Controller")
@RestController
@RequestMapping("/api/directors")
@RequiredArgsConstructor
@Tag(name = "Director Controller")
public class DirectorController {

  private final DirectorRepository directorRepository;
  private final DirectorMapper directorMapper;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Returns all directors")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public List<Director> getAllDirectors() {
    return directorRepository.findAll();
  }

  @GetMapping(value = "/{director_id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Returns director with given id")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "404", description = "Not Found"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public Director getDirector(
      @Parameter(description = "Id of Director", required = true, example = "1")
          @PathVariable("director_id")
          Integer directorId) {

    return findById(directorId);
  }

  @PostMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Adds new Director")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "201", description = "Created"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public ResponseEntity<Director> addDirector(
      @Parameter(description = "Director data", required = true) @RequestBody @Validated
          DirectorForm directorForm,
      UriComponentsBuilder b) {

    Director saved = directorRepository.save(directorMapper.map(directorForm));
    UriComponents uriComponents = b.path("/api/directors/{director_id}").buildAndExpand(saved.getId());

    return ResponseEntity.created(uriComponents.toUri()).body(saved);
  }

  @PutMapping(
      value = "/{director_id}",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Updates Director")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public Director updateDirector(
      @Parameter(description = "Id of Director", required = true, example = "1")
          @PathVariable("director_id")
          Integer directorId,
      @Parameter(description = "Director data", required = true) @RequestBody @Validated
          DirectorForm directorForm) {

    Director director = findById(directorId);

    return directorRepository.save(directorMapper.merge(director, directorForm));
  }

  @DeleteMapping(value = "/{director_id}")
  @Operation(summary = "Deletes Director")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "No Content"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public ResponseEntity<Void> deleteDirector(
      @Parameter(description = "Id of Director", required = true, example = "1")
          @PathVariable("director_id")
          Integer directorId) {

    checkIfExists(directorId);

    directorRepository.deleteById(directorId);

    return ResponseEntity.noContent().build();
  }

  private Director findById(Integer directorId) {
    return directorRepository
        .findById(directorId)
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Director not found by id - [%s]", directorId)));
  }

  private void checkIfExists(Integer directorId) {
    if (!directorRepository.existsById(directorId)) {
      throw new IllegalArgumentException(
          String.format("Director not found by id - [%s]", directorId));
    }
  }
}
