package pl.edu.pw.okno.dbaccessserver.actress;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.edu.pw.okno.dbaccessserver.model.Actress;

import javax.persistence.TableGenerator;
import java.util.List;

@Slf4j
@TableGenerator(name = "Actress Controller")
@RestController
@RequestMapping("/api/actresses")
@RequiredArgsConstructor
@Tag(name = "Actress Controller")
public class ActressController {

  private final ActressRepository actressRepository;
  private final ActressMapper actressMapper;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Returns all actresses")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public List<Actress> getAllActresses() {
    return actressRepository.findAll();
  }

  @GetMapping(value = "/{actress_id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Returns actress with given id")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "404", description = "Not Found"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public Actress getActress(
      @Parameter(description = "Id of Actress", required = true, example = "1")
          @PathVariable("actress_id")
          Integer actressId) {

    return findById(actressId);
  }

  @PostMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Adds new Actress")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "201", description = "Created"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public ResponseEntity<Actress> addActress(
      @Parameter(description = "Actress data", required = true) @RequestBody @Validated
          ActressForm actressForm,
      UriComponentsBuilder b) {

    Actress saved = actressRepository.save(actressMapper.map(actressForm));
    UriComponents uriComponents = b.path("/api/actresses/{actress_id}").buildAndExpand(saved.getId());

    return ResponseEntity.created(uriComponents.toUri()).body(saved);
  }

  @PutMapping(
      value = "/{actress_id}",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Updates Actress")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public Actress updateActress(
      @Parameter(description = "Id of Actress", required = true, example = "1")
          @PathVariable("actress_id")
          Integer actressId,
      @Parameter(description = "Actress data", required = true) @RequestBody @Validated
          ActressForm actressForm) {

    Actress actress = findById(actressId);

    return actressRepository.save(actressMapper.merge(actress, actressForm));
  }

  @DeleteMapping(value = "/{actress_id}")
  @Operation(summary = "Deletes Actress")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "No Content"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public ResponseEntity<Void> deleteActress(
      @Parameter(description = "Id of Actress", required = true, example = "1")
          @PathVariable("actress_id")
          Integer actressId) {

    checkIfExists(actressId);

    actressRepository.deleteById(actressId);

    return ResponseEntity.noContent().build();
  }

  private Actress findById(Integer actressId) {
    return actressRepository
        .findById(actressId)
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Actress not found by id - [%s]", actressId)));
  }

  private void checkIfExists(Integer actressId) {
    if (!actressRepository.existsById(actressId)) {
      throw new IllegalArgumentException(
          String.format("Actress not found by id - [%s]", actressId));
    }
  }
}
