package pl.edu.pw.okno.dbaccessserver.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity(name = "Film")
public class Movie {

  @Id
  @Column(name = "Id")
  @Schema(example = "1", description = "Id of the movie")
  private Integer id;

  @Column(name = "Tytul")
  @Schema(example = "Blade Runner", description = "Title of the movie")
  private String title;

  @Column(name = "[Rok Produkcji]")
  @Schema(example = "2018", description = "Year of production")
  private int year;

  @OneToOne
  @JoinColumn(name = "[Aktorka Id]", referencedColumnName = "Id")
  private Actress actress;

  @OneToOne
  @JoinColumn(name = "[Rezyser Id]", referencedColumnName = "Id")
  private Director director;
}
