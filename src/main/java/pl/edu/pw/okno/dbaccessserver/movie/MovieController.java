package pl.edu.pw.okno.dbaccessserver.movie;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.edu.pw.okno.dbaccessserver.model.Movie;

import javax.persistence.TableGenerator;
import java.util.List;

@Slf4j
@TableGenerator(name = "Movie Controller")
@RestController
@RequestMapping("/api/movies")
@RequiredArgsConstructor
@Tag(name = "Movie Controller")
public class MovieController {

  private final MovieRepository movieRepository;
  private final MovieMapper movieMapper;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Returns all movies")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public List<Movie> getAllMovies() {
    return movieRepository.findAll();
  }

  @GetMapping(value = "/{movie_id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Returns movie with given id")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "404", description = "Not Found"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public Movie getMovie(
      @Parameter(description = "Id of Movie", required = true, example = "1")
          @PathVariable("movie_id")
          Integer movieId) {

    return findById(movieId);
  }

  @PostMapping(
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Adds new Movie")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "201", description = "Created"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public ResponseEntity<Movie> addMovie(
      @Parameter(description = "Movie data", required = true) @RequestBody @Validated
          MovieForm movieForm,
      UriComponentsBuilder b) {

    Movie saved = movieRepository.save(movieMapper.map(movieForm));
    UriComponents uriComponents = b.path("/api/movies/{movie_id}").buildAndExpand(saved.getId());

    return ResponseEntity.created(uriComponents.toUri()).body(saved);
  }

  @PutMapping(
      value = "/{movie_id}",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Updates Movie")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Success"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public Movie updateMovie(
      @Parameter(description = "Id of Movie", required = true, example = "1")
          @PathVariable("movie_id")
          Integer movieId,
      @Parameter(description = "Movie data", required = true) @RequestBody @Validated
          MovieForm movieForm) {

    Movie movie = findById(movieId);

    return movieRepository.save(movieMapper.merge(movie, movieForm));
  }

  @DeleteMapping(value = "/{movie_id}")
  @Operation(summary = "Deletes Movie")
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "204", description = "No Content"),
        @ApiResponse(responseCode = "500", description = "Server failure")
      })
  public ResponseEntity<Void> deleteMovie(
      @Parameter(description = "Id of Movie", required = true, example = "1")
          @PathVariable("movie_id")
          Integer movieId) {

    checkIfExists(movieId);

    movieRepository.deleteById(movieId);

    return ResponseEntity.noContent().build();
  }

  private Movie findById(Integer movieId) {
    return movieRepository
        .findById(movieId)
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Movie not found by id - [%s]", movieId)));
  }

  private void checkIfExists(Integer movieId) {
    if (!movieRepository.existsById(movieId)) {
      throw new IllegalArgumentException(String.format("Movie not found by id - [%s]", movieId));
    }
  }
}
