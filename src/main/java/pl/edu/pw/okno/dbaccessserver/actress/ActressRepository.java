package pl.edu.pw.okno.dbaccessserver.actress;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pw.okno.dbaccessserver.model.Actress;

import java.util.Optional;

@Repository
public interface ActressRepository extends JpaRepository<Actress, Integer> {

  Optional<Actress> findByName(String name);
}
