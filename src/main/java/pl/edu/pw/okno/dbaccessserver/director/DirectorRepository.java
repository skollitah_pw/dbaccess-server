package pl.edu.pw.okno.dbaccessserver.director;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pw.okno.dbaccessserver.model.Director;

import java.util.Optional;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Integer> {

  Optional<Director> findByName(String name);
}
