package pl.edu.pw.okno.dbaccessserver.director;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class DirectorForm {

  @Schema(example = "Sylvester Stallone", description = "Director's name")
  @NotBlank
  private String name;

  @Schema(example = "1967", description = "Director's year of birth")
  @NotNull
  @Positive
  private int yearOfBirth;
}
