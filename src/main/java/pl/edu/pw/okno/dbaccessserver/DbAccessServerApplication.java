package pl.edu.pw.okno.dbaccessserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbAccessServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbAccessServerApplication.class, args);
	}
}
