package pl.edu.pw.okno.dbaccessserver.movie;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import pl.edu.pw.okno.dbaccessserver.actress.ActressRepository;
import pl.edu.pw.okno.dbaccessserver.director.DirectorRepository;
import pl.edu.pw.okno.dbaccessserver.model.Actress;
import pl.edu.pw.okno.dbaccessserver.model.Director;
import pl.edu.pw.okno.dbaccessserver.model.Movie;

import java.util.Random;

@RequiredArgsConstructor
@Controller
public class MovieMapper {

  private final ActressRepository actressRepository;
  private final DirectorRepository directorRepository;

  public Movie map(MovieForm movieForm) {
    return Movie.builder()
        .id(new Random().nextInt())
        .title(movieForm.getTitle())
        .year(movieForm.getYear())
        .actress(findActressByName(movieForm.getActressName()))
        .director(findDirectorByName(movieForm.getDirectorName()))
        .build();
  }

  public Movie merge(Movie movie, MovieForm movieForm) {
    movie.setTitle(movieForm.getTitle());
    movie.setYear(movieForm.getYear());
    movie.setActress(findActressByName(movieForm.getActressName()));
    movie.setDirector(findDirectorByName(movieForm.getDirectorName()));
    return movie;
  }

  private Actress findActressByName(String name) {
    return actressRepository
        .findByName(name)
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Actress not found by name - [%s]", name)));
  }

  private Director findDirectorByName(String name) {
    return directorRepository
        .findByName(name)
        .orElseThrow(
            () ->
                new IllegalArgumentException(
                    String.format("Director not found by name - [%s]", name)));
  }
}
