package pl.edu.pw.okno.dbaccessserver.config;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ComponentScan
@ControllerAdvice
@Slf4j
public class ExceptionMappingConfig {

  private static final String EXCEPTION_MESSAGE_LOG_TEMPLATE =
      "Request processing failed with exception";

  @ResponseBody
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<ErrorDto> handle(IllegalArgumentException exception) {
    return new ResponseEntity<>(new ErrorDto(exception.getMessage()), HttpStatus.NOT_FOUND);
  }

  @ResponseBody
  @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorDto> handle(Exception exception) {
    log.error(EXCEPTION_MESSAGE_LOG_TEMPLATE, exception);
    return new ResponseEntity<>(
        new ErrorDto(exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ResponseBody
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorDto> handle(MethodArgumentNotValidException exception) {
    return new ResponseEntity<>(
        new ErrorDto(String.join(", ", exception.getBindingResult().getFieldErrors().toString())),
        HttpStatus.BAD_REQUEST);
  }

  @Data
  private static class ErrorDto {

    private final String message;
  }
}
