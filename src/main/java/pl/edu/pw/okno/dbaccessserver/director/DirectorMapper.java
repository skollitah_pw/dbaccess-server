package pl.edu.pw.okno.dbaccessserver.director;

import org.springframework.stereotype.Controller;
import pl.edu.pw.okno.dbaccessserver.model.Director;

import java.util.Random;

@Controller
public class DirectorMapper {

  public Director map(DirectorForm directorForm) {
    return Director.builder()
        .id(new Random().nextInt())
        .name(directorForm.getName())
        .yearOfBirth(directorForm.getYearOfBirth())
        .build();
  }

  public Director merge(Director director, DirectorForm directorForm) {
    director.setName(directorForm.getName());
    director.setYearOfBirth(directorForm.getYearOfBirth());
    return director;
  }
}
